# [Advance Leetcode] User Interconnect

A application to demo how to connect back-end services (register and login and view user) to front-end involving login & register + views

## Microservices List

1. [This] User Interconnect (Views / Front-End)
2. [User Interconnect Service](https://gitlab.com/redevartk.advance-leetcode/user-interconnect-service) : For an API

## Properties

* Framework 		: Angular 13.0.0
* Programming Lang	: Typescript/Javascript

## Developer Note

* This is an old app using old method,
* For unknown reason the API_KEY doesn't work
* It have cors problem for now
* I don't intend to continue this project
* Mostly used for basic knowledge of interconnecting both front-end and back-end
